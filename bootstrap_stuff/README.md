The deploy.bash script deploys an bootstrapped Palo Alto firewall to specified ESXI. 

User must provide Palo Alto firewall ova file in the local directory under the name "PaloAlto.ova". 

Bootstrap configuration can be changed within bootstrap directory.  
Provided minimal configuration sets accessible management interface with the ip address of **10.56.0.110** and administrator user with login **admin** and password **Admin123**    
Bootstrap configuration guide: https://docs.paloaltonetworks.com/vm-series/9-0/vm-series-deployment/bootstrap-the-vm-series-firewall.html


**USAGE:**  
bash deploy.bash \<VM name> \<ESXI User> \<ESXI IP> \<ESXI PASS>

**REQUIREMENTS:**
*  OVFTOOL
*  sshpass
*  genisoimage
*  scp