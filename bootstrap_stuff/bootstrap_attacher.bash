underlinePrint() {
    UNDERLINE='________________________________________________________________\n'
    echo -e $UNDERLINE
    echo -e $1
    echo -e $UNDERLINE
}

mkdir bootstrap/config
mkdir bootstrap/content
mkdir bootstrap/license
mkdir bootstrap/plugins
mkdir bootstrap/software

#CONSTANTS

if [[ -z "${DATASTORE_DIR}" ]]; then
    DATASTORE_DIR=/vmfs/volumes/datastore1
else
    DATASTORE_DIR="${DATASTORE_DIR}"
fi

if [[ -z "${VM_NETWORK}" ]]; then
    VM_NETWORK="VM Network"
else
    VM_NETWORK="${VM_NETWORK}"
fi

if [[ -z "${BOOTSTRAP_DIR}" ]]; then
    BOOTSTRAP_DIR=bootstrap
else
    BOOTSTRAP_DIR="${BOOSTRATP_DIR}"
fi


VM_NAME=$1
ISO_NAME=$1_bootstrap.iso

ESXI_USER=$2
ESXI_HOST=$3
ESXI_PASS=$4

#SCRIPT

#Copy ISO file to ESXI
underlinePrint 'Copying ISO file to ESXI'
sshpass -p $ESXI_PASS scp $ISO_NAME $ESXI_USER@$ESXI_HOST:$DATASTORE_DIR/$VM_NAME
echo -e '\nResult: Iso filed copied'

#Attach bootstrap ISO to CD-ROM
underlinePrint 'Attaching bootstrap ISO to VM CD-ROM'

VMX_FILEPATH=$DATASTORE_DIR/$VM_NAME/$VM_NAME.vmx
sshpass -p $ESXI_PASS ssh $ESXI_USER@$ESXI_HOST "sed -i 's|ide1:0.deviceType.*|ide1:0.deviceType = \"cdrom-image\"|g' $VMX_FILEPATH; 
sed -i 's|ide1:0.fileName.*|ide1:0.fileName = \"$DATASTORE_DIR\\$VM_NAME\\$ISO_NAME\"|g' $VMX_FILEPATH; 
sed -i 's|ide1:0.present.*|ide1:0.present = \"TRUE\"|g' $VMX_FILEPATH;
sed -i 's|ide1:0.startConnected.*|ide1:0.startConnected = \"TRUE\"|g' $VMX_FILEPATH;
sed -i '/ide1:0.clientDevice.*/d' $VMX_FILEPATH"

echo -e '\nResult: Boostrap attached'

underlinePrint 'Script finished'
