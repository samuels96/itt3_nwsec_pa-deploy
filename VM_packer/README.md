The VM_packer.bash script builds an VMX image and vagrant box from an existing VM machine on existing ESXI. The source VM machine must be in shutdown state for the script to work.

The VMX image will be stored on the ESXI while the vagrant box will be saved localy in the vagrant_boxes directory.

**USAGE:**  
bash pack_VM.bash \<Source VM name>  \<(new) VM name> \<ESXI User> \<ESXI IP> \<ESXI PASS>

**REQUIREMENTS:**
* packer 
* sshpass
* scp